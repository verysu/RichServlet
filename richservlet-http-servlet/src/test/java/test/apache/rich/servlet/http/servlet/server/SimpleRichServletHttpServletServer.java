/**
 * 
 */
package test.apache.rich.servlet.http.servlet.server;

import com.apache.rich.servlet.core.server.helper.RichServletServerOptions;
import com.apache.rich.servlet.core.server.monitor.RichServletServerMonitor;
import com.apache.rich.servlet.http.servlet.server.RichServletHttpServletServerProvider;
import com.apache.rich.servlet.http.servlet.realize.RichServletHttpServlet;
import com.apache.rich.servlet.http.servlet.realize.configuration.RichServletHttpServletConfiguration;
import test.apache.rich.servlet.http.servlet.TestServlet;
import com.apache.rich.servlet.http.servlet.realize.configuration.RichServletHttpWebappConfiguration;

/**
 * @author wanghailing
 *
 */
public class SimpleRichServletHttpServletServer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		RichServletHttpWebappConfiguration configuration=new RichServletHttpWebappConfiguration();
		configuration.addServletConfigurations(new RichServletHttpServletConfiguration(TestServlet.class,"/testServlet"));
		RichServletHttpServlet servletHttpServlet=new RichServletHttpServlet(configuration);
		com.apache.rich.servlet.http.servlet.server.RichServletHttpServletServer httpServletServer=RichServletHttpServletServerProvider.getInstance().service(servletHttpServlet);
		httpServletServer.disableInternalController();
        // disable stats monitor
        RichServletServerMonitor.disable();

        // 2. choose http params. this is unnecessary
        httpServletServer.option(RichServletServerOptions.IO_THREADS, Runtime.getRuntime().availableProcessors())
                .option(RichServletServerOptions.WORKER_THREADS, 128)
                .option(RichServletServerOptions.TCP_BACKLOG, 1024)
                .option(RichServletServerOptions.TCP_NODELAY, true)
                .option(RichServletServerOptions.TCP_TIMEOUT, 10000L)
                .option(RichServletServerOptions.TCP_HOST,"127.0.0.1")
                .option(RichServletServerOptions.TCP_PORT,8080)
                .option(RichServletServerOptions.MAX_CONNETIONS, 4096);
     // 3. start http server
        if (!httpServletServer.start()){
            System.err.println("HttpServer run failed");
        } 
        try {
            // join and wait here
        		httpServletServer.join();
        		httpServletServer.shutdown();
        } catch (InterruptedException ignored) {
        }
	}

}
