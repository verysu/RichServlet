
package com.apache.rich.servlet.http.servlet.realize;


import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.apache.rich.servlet.http.servlet.realize.configuration.RichServletHttpServletConfiguration;

import com.apache.rich.servlet.http.servlet.realize.configuration.RichServletHttpFilterConfiguration;

import java.io.IOException;
import java.util.LinkedList;

/**
 * http filter chain
 * @author wanghailing 
 *
 */
public class RichservletHttpFilterChain implements FilterChain {

    private LinkedList<RichServletHttpFilterConfiguration> filterConfigurations;

    private RichServletHttpServletConfiguration servletConfiguration;

    public RichservletHttpFilterChain(RichServletHttpServletConfiguration servletConfiguration) {
        this.servletConfiguration = servletConfiguration;
    }

    public void addFilterConfiguration(RichServletHttpFilterConfiguration config) {

        if (this.filterConfigurations == null)
            this.filterConfigurations = new LinkedList<RichServletHttpFilterConfiguration>();

        this.filterConfigurations.add(config);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {

    		RichServletHttpFilterConfiguration config = filterConfigurations != null ? filterConfigurations
                .poll()
                : null;

        if (config != null){
            config.getHttpComponent().doFilter(request, response, this);
        }else if (this.servletConfiguration != null) {

            this.servletConfiguration.getHttpComponent().service(request,
                    response);
        }
    }

    public boolean isValid() {
        return this.servletConfiguration != null
                || (this.filterConfigurations != null && !this.filterConfigurations
                .isEmpty());
    }

    public RichServletHttpServletConfiguration getServletConfiguration() {
        return servletConfiguration;
    }

}
