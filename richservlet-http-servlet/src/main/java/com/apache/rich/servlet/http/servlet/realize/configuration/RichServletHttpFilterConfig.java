/**
 * 
 */
package com.apache.rich.servlet.http.servlet.realize.configuration;

import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;

import com.apache.rich.servlet.http.servlet.realize.RichServletHttpServletContext;

import com.apache.rich.servlet.http.servlet.realize.adapter.ConfigAdapter;

/**
 * @author wanghailing
 *
 */
public class RichServletHttpFilterConfig extends ConfigAdapter implements
		FilterConfig {

	public RichServletHttpFilterConfig(String ownerName) {
		super(ownerName);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.FilterConfig#getFilterName()
	 */
	@Override
	public String getFilterName() {
		return super.getOwnerName();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.FilterConfig#getServletContext()
	 */
	@Override
	public ServletContext getServletContext() {
		return RichServletHttpServletContext.get();
	}

}
