/**
 * 
 */
package com.apache.rich.servlet.http2.server;

import com.apache.rich.servlet.http2.server.acceptor.RichServletHttp2ServerAsyncAcceptor;


/**
 * @author wanghailing
 *
 */
public class RichServletHttp2ServerProvider {
	
	private static class LazyHolder {
		private static final RichServletHttp2ServerProvider INSTANCE = new RichServletHttp2ServerProvider();
	}

	private RichServletHttp2ServerProvider() {

	}

	public static final RichServletHttp2ServerProvider getInstance() {
		return LazyHolder.INSTANCE;
	}

	
	public RichServletHttp2Server service() {
			RichServletHttp2Server httpServer = new RichServletHttp2Server();
            httpServer.ioAcceptor(new RichServletHttp2ServerAsyncAcceptor(httpServer));
            return httpServer;
	}
}
