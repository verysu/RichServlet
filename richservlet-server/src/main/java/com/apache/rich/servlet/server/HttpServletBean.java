/**
 * 
 */
package com.apache.rich.servlet.server;

import java.io.Serializable;
import javax.servlet.http.HttpServlet;

/**
 * @author wanghailing
 *
 */
public class HttpServletBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 400411596893199339L;
	
	/**
	 * servlet  路径
	 */
	private String servletpath;
	
	/**
	 * http servlet
	 */
	private Class<? extends HttpServlet> httpServlet;
	
	
	
	/**
	 * @return the servletpath
	 */
	public String getServletpath() {
		return servletpath;
	}

	/**
	 * @param servletpath the servletpath to set
	 */
	public void setServletpath(String servletpath) {
		this.servletpath = servletpath;
	}

	/**
	 * @return the httpServlet
	 */
	public Class<? extends HttpServlet> getHttpServlet() {
		return httpServlet;
	}

	/**
	 * @param httpServlet the httpServlet to set
	 */
	public void setHttpServlet(Class<? extends HttpServlet> httpServlet) {
		this.httpServlet = httpServlet;
	}

	
	
}
